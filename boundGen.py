import fileinput
import random

for line in fileinput.input("test.txt", inplace=True):
    random.seed(fileinput.filelineno());
    i = random.randint(1, 20);
    print "%s %d\n" % (line.rstrip(), i),