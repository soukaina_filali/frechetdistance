
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.Map.Entry;

import core.DataHolder;
import core.ParallelQueryService;
import core.QueryExecutor;
import util.Pair;
import util.Trajectory;

public class Runner {

	static String pathPrefix = null;
	static Path datasetPath;
	static Path queriesPath;
	static Path dataFilesPath;
	static Path ResultFilesPath;
	static String fileSeparator = " ";

	static final boolean IS_TAXI = false;

	public static void initialize() throws FileNotFoundException {
		pathPrefix = (System.getProperty("user.dir"));
		datasetPath = Paths.get(pathPrefix, "dataset.txt");
		queriesPath = Paths.get(pathPrefix, "queries.txt");
		dataFilesPath = Paths.get(pathPrefix, "files/");
		ResultFilesPath = Paths.get(pathPrefix, "Results/");

		if (IS_TAXI) {
			datasetPath = Paths.get(pathPrefix, "Tdataset.txt");
			queriesPath = Paths.get(pathPrefix, "test_1.txt");
			dataFilesPath = Paths.get(pathPrefix, "Tdrive/");
			fileSeparator = ",";
		}
	}

	public static void main(String[] args) throws Exception {

		initialize();
		System.out.println("Start Reading");

		// Reading dataset files from folder and index
		readDataset();

		// Read the queries
		readQueryTrajectories();

		double meanTrajectoryLength = DataHolder.totalTrajectoriesLength / DataHolder.datasetSize;
		long startTime = System.currentTimeMillis();
		ParallelQueryService parallelQueryService = new ParallelQueryService();
		Iterator<Entry<String, Pair<Double, Integer>>> iterator = DataHolder.queryBoundsMap.entrySet().iterator();
		while (iterator.hasNext()) {
			int queryLoadCount = (int) Math.ceil(138.95 * Math.exp(-0.003 * meanTrajectoryLength));

			System.out.println("The mean trajectory Length is " + meanTrajectoryLength);
			QueryExecutor queryExecutor = new QueryExecutor();
			while (queryLoadCount > 0 && iterator.hasNext()) {

				Entry<String, Pair<Double, Integer>> queryEntry = iterator.next();
				String queryTrajectoryKey = queryEntry.getKey();
				Double leashBound = queryEntry.getValue().first;
				Integer queryline = queryEntry.getValue().second;

				queryExecutor.add(queryTrajectoryKey, leashBound, queryline);
				queryLoadCount--;
			}
			parallelQueryService.addQueryToExecutor(queryExecutor);
		}

		parallelQueryService.awaitTermination();
		long diffTime = System.currentTimeMillis() - startTime;

		System.out.println("The Number of Simplified Trajectories is =" + DataHolder.simplifieTrajectoriesdcount);
		System.out.println("The queries took " + diffTime + " milliseconds");

		// uncomment to display results in the console log
		System.out.println("RESULTS::");

		for (Entry<String, Pair<Integer, TreeSet<String>>> entry :

		DataHolder.results.entrySet()) {

			System.out.println("Query #: " + entry.getValue().first +
					"  Trajectory: " + entry.getKey() + "	= "
					+ entry.getValue().second.toString());
		}
		
		System.out.println("Writing Results to Files.....");
		File resultsDirectory = ResultFilesPath.toAbsolutePath().toFile();
		if(!resultsDirectory.exists()){
			resultsDirectory.mkdirs();
		}
		for (Entry<String, Pair<Integer,TreeSet<String>>> entry : DataHolder.results.entrySet()) {
			
			Pair<Integer, TreeSet<String>> pair_result = entry.getValue();
				
			// Format the result file name to match the contest requirements
			String resultFilePrefix = "";
			int counterlength = (int) (Math.log10(pair_result.first) + 1);
			int zeros = 6 - counterlength;
			for (int i = 0; i < zeros; i++)
				resultFilePrefix += "0";
			
			Path resultFilePath= Paths.get(ResultFilesPath.toString(), 
					"result-" + resultFilePrefix + pair_result.first + ".txt");
			
			if(resultFilePath.toFile().createNewFile()){
				PrintWriter outputfile = new PrintWriter(resultFilePath.toString(), "UTF-8");

				
				for (String result : pair_result.second) { 
					outputfile.println(result);
				}
				outputfile.flush();
				outputfile.close();
			}
		}
	}

	private static void readQueryTrajectories() throws IOException {

		BufferedReader queries = new BufferedReader(new FileReader(queriesPath.toString()));
		int linecount = 1; 
		String queryLine = null;
		
		while ((queryLine = queries.readLine()) != null) {
			String[] params = queryLine.split(" ");
			String queryTrajectoryId = params[0];
			Double leashBound = new Double(params[1]);
			String queryTrajectoryKey = queryTrajectoryId + "_" + leashBound;
			DataHolder.queryBoundsMap.put(queryTrajectoryKey, new Pair<Double, Integer>(leashBound, linecount));
			DataHolder.results.put(queryTrajectoryId + "_" + leashBound, new Pair<Integer, TreeSet<String>>(null,null));
			
			linecount++;
		}
		queries.close();

	}

	private static void readDataset() throws IOException, FileNotFoundException {
		String filename = "";
		BufferedReader datasetReader = new BufferedReader(new FileReader(datasetPath.toString()));
		while ((filename = datasetReader.readLine()) != null) {
			System.out.print("Reading File..   " + dataFilesPath + "/" + filename);

			Path filePath = Paths.get(dataFilesPath.toString(), filename);

			Trajectory trj = readTrajectory(filePath); // read trajectory from
			// file
			if (trj != null) {
				System.out.println("\tSize: " + trj.size());
				DataHolder.totalTrajectoriesLength += trj.size();
				DataHolder.trajectoryMap.put(filename, trj); // put it into map
				DataHolder.longestEdgeMap.put(filename, Trajectory.getLongestEdge(trj));
				DataHolder.rTreeIndex.insert(trj.getStartPoint(), trj.getEndPoint(), filename);
			}

		}
		datasetReader.close();
		DataHolder.datasetSize = DataHolder.trajectoryMap.size();
	}

	private static Trajectory readTrajectory(Path filePath) throws IOException, FileNotFoundException {
		ArrayList<Double> xCoordinates = new ArrayList<>();
		ArrayList<Double> yCoordinates = new ArrayList<>();

		try (BufferedReader br = new BufferedReader(new FileReader(filePath.toString()))) {
			String line;
			br.readLine(); // skip the first line
			while ((line = br.readLine()) != null) {
				String[] values = line.split(fileSeparator);
				xCoordinates.add(new Double(values[0])); // this is the X
				// coordinate
				yCoordinates.add(new Double(values[1])); // this is the Y
				// coordinate
			}
		}
		if (xCoordinates.size() == 0) {
			return null;
		}
		// convert it to 2D double array
		double[][] pointSequence = new double[xCoordinates.size()][2];
		for (int i = 0; i < xCoordinates.size(); i++) {
			pointSequence[i][0] = xCoordinates.get(i);
			pointSequence[i][1] = yCoordinates.get(i);
		}
		return new Trajectory(pointSequence);
	}
}