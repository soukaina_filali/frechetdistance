package core;

import util.DualRTree;
import util.Trajectory;

import java.util.Collection;
import java.util.HashMap;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import util.Pair;

/**
 * Created by ahmetkucuk on 7/19/17.
 */
public class DataHolder {

	public static int datasetSize;
	public static int totalTrajectoriesLength;
	public static int simplifieTrajectoriesdcount;
	public static double simplificationError;
	
    public final static HashMap<String, Trajectory> trajectoryMap = new HashMap<>();
    
    
    public final static HashMap<String, Double> longestEdgeMap = new HashMap<>();
    public final static DualRTree rTreeIndex = new DualRTree();
    public final static HashMap<String, Pair<Double, Integer>> queryBoundsMap = new HashMap<>();
    public final static ConcurrentHashMap<String, Pair<Integer,TreeSet<String>>> results = new ConcurrentHashMap<>();

	public static double calculateSimplificationError(double ratio) {
		double totalMaxEdge = 0.0;
		for(Double edgeLength : longestEdgeMap.values()){
			totalMaxEdge += edgeLength;
		}
		return (ratio * totalMaxEdge) / (double)longestEdgeMap.size();
	}
}
