package core;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by ahmetkucuk on 7/19/17.
 */
public class ParallelQueryService {

    public static final int THREAD_COUNT =  Runtime.getRuntime().availableProcessors() -1;
    private static ExecutorService executorService;

    public ParallelQueryService() {
    	System.out.println("Number of Threads = "+ THREAD_COUNT);
        executorService = Executors.newFixedThreadPool(THREAD_COUNT);
    }

    public void addQueryToExecutor(QueryExecutor queryExecutor) {
        executorService.execute(queryExecutor);
    }

    public void awaitTermination() {
        executorService.shutdown();
        try {
            executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {
        }
    }
}
