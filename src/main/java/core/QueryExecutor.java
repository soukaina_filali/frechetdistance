package core;

import frechet.ContinuousFrechet;
import frechet.DiscreteFrechet;
import core.DataHolder;
import util.Triple;
import util.Pair;
import util.Trajectory;

import java.util.*;

/**
 * Created by ahmetkucuk on 7/19/17.
 * @author berkay
 */
public class QueryExecutor implements Runnable {



	public List<Triple<String, Double, Integer>> triples = new ArrayList<>();

	public void add(String queryTrajectoryKey, Double leashBound, Integer linenumber) {
		triples.add(new Triple<>(queryTrajectoryKey, leashBound, linenumber));
	}

	public void run() {
		try {
			for(Triple<String, Double, Integer> p: triples) {
				computeFrechet(p.first, p.second, p.third);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void computeFrechet(String queryTrajectoryKey, Double leashBound, Integer line) throws Exception{
		System.out.println("Processing " + queryTrajectoryKey);
		String queryTrajectoryId = queryTrajectoryKey.split("_")[0];
		Trajectory queryTrajectory = DataHolder.trajectoryMap.get(queryTrajectoryId);
		// Start Filtering
		Set<String> tempResults = new TreeSet<String>();
		Set<String> candidateTrajectories=null;

		boolean rtreeFilterOn = true;
		boolean dfdFilterOn = true;

		if(rtreeFilterOn){
			candidateTrajectories = DataHolder.rTreeIndex.query(
					queryTrajectory.getStartPoint(),
					queryTrajectory.getEndPoint(), leashBound);

			// Uncomment to display R-tree Filter Statistics
			/*	System.out.println("R-Tree Filter Effectiveness: " + 
					(double)candidateTrajectories.size()*100.0/(double)DataHolder.datasetSize
					+ "%");
			System.out.println("R-Tree filtered candidate count: "+ candidateTrajectories.size());*/

			Set<String> dfdPositive = new HashSet<>();
			Set<String> dfdNegative = new HashSet<>();
			if(dfdFilterOn){
				for(String resultId : candidateTrajectories){

					double dfd = DiscreteFrechet.computeDiscreteFrechet(queryTrajectory, DataHolder.trajectoryMap.get(resultId));
					// ContFrechet <= DFD
					if(leashBound > dfd){ // then it is also greater than ContFrechet, so add
						dfdPositive.add(resultId);
					} else{
						//Let maxL be max(L(traj1), L(traj2)), where L is the longest edge
						//dfd <= contFrechet + maxL
						// --> dfd - maxL <= contFrecehet
						double maxL = Math.max(DataHolder.longestEdgeMap.get(queryTrajectoryId),
								DataHolder.longestEdgeMap.get(resultId));
						if(leashBound < dfd - maxL){ // then it is also less than ContFrechet, so remove
							dfdNegative.add(resultId);
						}
					}
				}
			}
			// Uncomment to display DFD Filter Statistics
			/*	System.out.println("DFD-Neg Filter Effectiveness: " + 
					(double)dfdNegative.size()*100.0/(double)DataHolder.datasetSize
					+ "%(" + dfdNegative.size() + " removed)");
			System.out.println("DFD-Pos Filter Effectiveness: " + 
					((double)dfdPositive.size()*100.0/(double)DataHolder.datasetSize)
					+ "% (" + dfdPositive.size() + " qualified)");*/
			candidateTrajectories.removeAll(dfdNegative);
			candidateTrajectories.removeAll(dfdPositive);
			tempResults.addAll(dfdPositive);

		} else{
			candidateTrajectories = DataHolder.trajectoryMap.keySet();
		}


		for(String trajId : candidateTrajectories){
			Trajectory trj = DataHolder.trajectoryMap.get(trajId);
			boolean withinBound = ContinuousFrechet.frechetDecision(queryTrajectory, trj, leashBound);
			if(withinBound){
				tempResults.add(trajId);
			}
		}
		
		DataHolder.results.put(queryTrajectoryKey, new Pair<Integer,TreeSet<String>>(line, (TreeSet<String>) tempResults));
		//DataHolder.results.get(queryTrajectoryKey).addAll(tempResults);
		System.out.println(queryTrajectoryId + " is finished. \n" );
	}
}
