package frechet;

import util.Trajectory;

public class DiscreteFrechet{

	public static boolean discreteFrechetDecision(Trajectory timeSeriesP, Trajectory timeSeriesQ, double leash) {
		
			double res = computeDiscreteFrechet(timeSeriesP, timeSeriesQ);
			
			if (res<= leash)
				return true;
			
			return false;
	}

	/**
	 * Wraper that initializes the mem array to -1 values and calls the computeDFD.
	 * 
	 * 
	 * @param traj1
	 * 				first input trajectory
	 * @param traj2
	 * 				second input trajectory
	 * 
	 * @return The minimum distance that can traverse traj1 and traj2
	 *         
	 */
	public static double computeDiscreteFrechet(Trajectory traj1, Trajectory traj2) {
		
		double[][] mem;
		mem = new double[traj1.size()][traj2.size()];

		// initialize all values to -1
		for (int i = 0; i < mem.length; i++) {
			for (int j = 0; j < mem[i].length; j++) {
				mem[i][j] = -1.0;
			}
		}
		return computeDFD(traj1.size() - 1, traj2.size() - 1, traj1 , traj2, mem);
	}

	/**
	 * Compute the Discrete Frechet Distance (DFD) given the index locations of
	 * i and j. In this case, the bottom right hand corner of the mem.
	 * 
	 *  This method uses dynamic programming to improve performance.
	 * 
	 * Pseudocode of computing DFD from page 5 of
	 * http://www.kr.tuwien.ac.at/staff/eiter/et-archive/cdtr9464.pdf
	 * 
	 * @param i  
	 * 			the row
	 * @param j  
	 * 			the column
	 * 
	 * @return The minimum distance that can traverse traj1 and traj2
	 */
	private static double computeDFD(int i, int j, Trajectory traj1, Trajectory traj2, double[][] mem) {

		// if the value has already been solved
		if (mem[i][j] > -1)
			return mem[i][j];
		// if top left column, just compute the distance
		else if (i == 0 && j == 0)
			mem[i][j] = traj1.get(i).distance(traj2.get(j));
		// can either be the actual distance or distance pulled from above
		else if (i > 0 && j == 0)
			mem[i][j] = getMaxValue(computeDFD(i - 1, 0,  traj1, traj2, mem), traj1.get(i).distance(traj2.get(j)));
		// can either be the distance pulled from the left or the actual
		// distance
		else if (i == 0 && j > 0)
			mem[i][j] = getMaxValue(computeDFD(0, j - 1, traj1, traj2, mem), traj1.get(i).distance(traj2.get(j)));
		// can be the actual distance, or distance from above or from the left
		else if (i > 0 && j > 0) {
			mem[i][j] = getMaxValue(getminValue(computeDFD(i - 1, j, traj1, traj2, mem), computeDFD(i - 1, j - 1 ,traj1, traj2, mem),
					computeDFD(i, j - 1, traj1, traj2, mem)),
					traj1.get(i).distance(traj2.get(j)));
		}
		// infinite
		else
			mem[i][j] = Integer.MAX_VALUE;

		return mem[i][j];
	}

	/**
	 * Retrieves the min value of the input values
	 * 
	 * @param values - the input values
	 * 
	 * @return The minimum value of the input values
	 */
	public static double getminValue(double... values) {
		double min = Integer.MAX_VALUE;
		for (double j : values) {
			if (j <= min)
				min = j;
		}
		return min;
	}
	/**
	 * Retrieves the Max value of the input values
	 * 
	 * @param values - the input values 
	 * 
	 * @return The max value of the input values.
	 */
	public static double getMaxValue(double... values) {
		double max = Integer.MIN_VALUE;
		for (double j : values) {
			if (j >= max)
				max = j;
		}
		return max;
	}
}