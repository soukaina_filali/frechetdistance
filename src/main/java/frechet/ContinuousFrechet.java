package frechet;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;
import util.Pair;
import util.Trajectory;


public class ContinuousFrechet {

	public static boolean frechetDecision(Trajectory traj1, Trajectory traj2, double leash)
			throws Exception {
		// Calculating the Euclidean Distance Grid between traj1 and
		// traj2 points squared
		double[][] distMatrixSq12 = new double[traj1.size()][traj2.size()];
		int length1 = traj1.size();
		int length2 = traj2.size();

		for (int i = 0; i < traj1.size(); i++) {
			for (int j = 0; j < traj2.size(); j++) {
				//TODO parallelize here if you can
				distMatrixSq12[i][j] = distanceSquared(traj1.get(i), traj2.get(j)); //can be replaced
			}
		}

		// Validate Trajectories
		validateTrajectories(distMatrixSq12, leash, length1, length2);

		// Calculate the Euclidean distance between consecutive points of the 2
		// trajectories
		double[] dist1 = new double[length1 - 1];
		double[] dist2 = new double[length2 - 1];

		for (int i = 0; i < length1 - 1; i++) {
			dist1[i] = traj1.get(i).distance(traj1.get(i + 1));
		}

		for (int i = 0; i < length2 - 1; i++) {
			dist2[i] = traj2.get(i).distance(traj2.get(i + 1));
		}

		if (checkFrechetLeash(length1, length2, leash, traj1, traj2, dist1, dist2, distMatrixSq12)) {
			// System.out.println("TRUE");
			return true;
		} else {
			// System.out.println("FALSE");
			return false;
		}

	}

	/**
	 * Trajectory validation, it throws an exception if one of the trajectory
	 * has no point coordinates. The function return false if the leash is not
	 * working between the trajectories start points and endpoints.
	 * 
	 * @param distMatrixSq12 
	 * 				pairwise distance matrix betweent the point coordinate of traj1 and traj2.
	 * @param leash
	 * 				user-defined parameter, which corresponds to the main.java.Frechet bound
	 * @param length1
	 * 				length of traj1
	 * @param length2
	 * 				length of traj2
	 * @return
	 * 		input trajectories traj1 and traj2 are valid
	 * @throws Exception
	 */
	public static boolean validateTrajectories(double[][] distMatrixSq12, double leash, int length1, int length2)
			throws Exception {
		double leashSq = leash * leash;
		if (length1 == 0 || length2 == 0) {
			throw new Exception("One(or both) trajectories contains 0 points");
		} else if (leashSq < distMatrixSq12[0][0] || leashSq < distMatrixSq12[length1 - 1][length2 - 1]) {
			/*
			 * System.out.
			 * println("The given leash can not work:(One of the Euclidean dist"
			 * + " of [0][0] and [length1][length2] is higher than the leash)");
			 */
			return false;
		}
		return true;
	}


	/**
	 * This is the Frechet Decision Problem that checks whether the leash
	 * parameter can be used to traverse the 2 curves
	 * 
	 * @param length1
	 * 				length of traj1
	 * @param length2
	 * 				length of traj2
	 * @param leash
	 * 				user-defined parameter, which corresponds to the Frechet bound
	 * @param traj1
	 * 				first input trajctory
	 * @param traj2
	 * 				Second input trajctory
	 * @param dist1
	 * 				distance between consecutives point of traj1
	 * @param dist2
	 * 				distance between consecutives point of traj2
	 * @return
	 * @throws Exception
	 */
	static boolean checkFrechetLeash(int length1, int length2, double leash, Trajectory traj1,
			Trajectory traj2, double[] dist1, double[] dist2, double[][] distMatrixSq12) throws Exception {
		double leashSq = leash * leash;
		// Calculation if either trajectory has only one point.
		if (length1 == 1 | length2 == 1) {
			double leashlength = singlePointCalc2(traj1, traj2);
			if (leash >= leashlength) {
				return true;
			} else {
				return false;
			}
		}
		Double[][][] bottom = computeFreeSpaceBottom2(traj1, traj2, dist1, distMatrixSq12, leashSq);
		Double[][][] left = computeFreeSpaceLeft2(traj1, traj2, dist2, distMatrixSq12, leashSq);

		// Initialize the bottom and left arrays to -1
		Double[][][] newBot = new Double[2][length1 - 1][length2];
		Double[][][] newLeft = new Double[2][length1][length2 - 1];

		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < length1 - 1; j++) {
				for (int k = 0; k < length2; k++) {
					newBot[i][j][k] = -1.0;
				}
			}
		}

		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < length1; j++) {
				for (int k = 0; k < length2 - 1; k++) {
					newLeft[i][j][k] = -1.0;
				}
			}
		}

		// Setting up the new arrays to find the monotone freespace.
		newLeft[0][0][0] = left[0][0][0];
		newLeft[1][0][0] = left[1][0][0];

		newBot[0][0][0] = bottom[0][0][0];
		newBot[1][0][0] = bottom[1][0][0];

		// Setting the first line of the new left array.
		if (length2 > 2) {
			for (int point2 = 1; point2 < (length2 - 1); point2++) {
				if (newLeft[1][0][(point2 - 1)] == 1) {
					newLeft[0][0][point2] = left[0][0][point2];
					newLeft[1][0][point2] = left[1][0][point2];
				}
			}
		}
		// Setting the first line of the new bottom array.
		if (length1 > 2) {
			for (int point1 = 1; point1 < (length1 - 1); point1++) {
				if (newBot[1][(point1 - 1)][0] == 1) {
					newBot[0][point1][0] = bottom[0][point1][0];
					newBot[1][point1][0] = bottom[1][point1][0];
				}
			}
		}

		Pair<Double[][],Double[][]> Returns = computeMonotoneFreeSpace(length1, length2, bottom, left, newBot, newLeft);
		newLeft[1] = Returns.first;
		newBot[1] = Returns.second;
		/*
		 * System.out.println("Final New BOTtom 1"); for(int y=
		 * 0;y<length1-1;y++){ for(int k= 0;k<length2;k++){
		 * System.out.print(" "+newBot[0][y][k]); } System.out.println(); }
		 * System.out.println("New Bottom 2"); for(int y= 0;y<length1-1;y++){
		 * for(int k= 0;k<length2;k++){ System.out.print(" "+newBot[1][y][k]); }
		 * System.out.println(); }
		 * 
		 * System.out.println("New LEFT 1"); for(int y= 0;y<length1;y++){
		 * for(int k= 0;k<length2-1;k++){
		 * System.out.print(" "+newLeft[0][y][k]); } System.out.println(); }
		 * System.out.println("New LEFT 2"); for(int y= 0;y<length1;y++){
		 * for(int k= 0;k<length2-1;k++){
		 * System.out.print(" "+newLeft[1][y][k]); } System.out.println(); }
		 */
		// If the monotone freespace reaches the final point then
		// the leash is successful.
		if (newLeft[1][length1 - 1][(length2 - 2)] == 1 | newBot[1][(length1 - 2)][length2 - 1] == 1) {
			return true;
		} else {
			// Otherwise the leash is unsuccessful.
			return false;
		}
	}

	/**
	 * Computes the Free Space of 1 trajectory with respect to another one
	 * 
	 * @param traj1
	 * 				first input trajctory
	 * @param traj2
	 * 				Second input trajctory
	 * @param dist1
	 * 				distance between consecutives point of traj1
	 * @param dist2
	 * 				distance between consecutives point of traj2
	 */
	public static Double[][][] computeFreeSpaceBottom2(Trajectory traj1, Trajectory traj2, double[] dist1,
			double[][] distMatrixSq12, double leashSq) {
		// Creating a unit vector in the direction of the next point from point1.
		Point unitVector1 = new GeometryFactory().createPoint(new Coordinate(0, 0));
		Point vector12 = new GeometryFactory().createPoint(new Coordinate(0, 0));

		int length1 = traj1.size();
		int length2 = traj2.size();
		Double[][][] bottom = new Double[2][length1 - 1][length2];

		// initialize the bottom Array
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < length1 - 1; j++) {
				for (int k = 0; k < length2; k++) {
					bottom[i][j][k] = -1.0;
				}
			}
		}

		for (int point1 = 0; point1 < (length1 - 1); point1++) {
			if (dist1[point1] != 0) {
				//				unitV1.setLocation(
				//						1.0 * (traj1.get(point1 + 1).getX() - traj1.get(point1).getX()) / (dist1[point1]),
				//						1.0 * (traj1.get(point1 + 1).getY() - traj1.get(point1).getY()) / (dist1[point1]));
				//				
				double uv_i = 1.0 * (traj1.get(point1 + 1).getX() - traj1.get(point1).getX()) / (dist1[point1]);
				double uv_j = 1.0 * (traj1.get(point1 + 1).getY() - traj1.get(point1).getY()) / (dist1[point1]);
				unitVector1 = new GeometryFactory().createPoint(new Coordinate(uv_i,uv_j)); 
			}
			for (int point2 = 0; point2 < length2; point2++) {
				// Creating a vector from point1 to point 2.
				//				vect12.setLocation(
				//						traj2.get(point2).getX() - traj1.get(point1).getX(),
				//						traj2.get(point2).getY() - traj1.get(point1).getY());
				double v_i = traj2.get(point2).getX() - traj1.get(point1).getX();
				double v_j = traj2.get(point2).getY() - traj1.get(point1).getY();
				vector12 = new GeometryFactory().createPoint(new Coordinate(v_i,v_j));
				// System.out.println("The vect12 is "+vect12);
				// Dot product finds how far from point1 the closest point on
				// the line is.
				double pointDist = unitVector1.getX() * vector12.getX() + unitVector1.getY() * vector12.getY();
				//System.out.println("POint dist"+ pointDist);
				// Squaring for easy calculations
				double pointDistSq = pointDist * pointDist;
				// The square of the distance between the line segment and the
				// point.
				double shortDist = distMatrixSq12[point1][point2] - pointDistSq;
				// System.out.println("SHort dist "+ shortDist);
				// If some part of the current line can be used by the leash.
				if (shortDist <= leashSq) {
					// Calculating the envelope along the line.
					double envSize = Math.sqrt(leashSq - shortDist);
					double envelopeLow = pointDist - envSize;
					double envelopeHigh = pointDist + envSize;
					// If the whole line is within the envelope.
					if (envelopeHigh >= dist1[point1] & envelopeLow <= 0) {
						bottom[0][point1][point2] = 0.0;
						bottom[1][point1][point2] = 1.0;
					} else if (envelopeHigh >= 0 & envelopeLow <= 0) {
						// If the start of the line is within the envelope.
						bottom[0][point1][point2] = 0.0;
						bottom[1][point1][point2] = 1.0 * envelopeHigh / dist1[point1];
					} else if (envelopeHigh >= dist1[point1] & envelopeLow <= dist1[point1]) {
						// If the end of the line is within the envelope.
						bottom[0][point1][point2] = 1.0 * envelopeLow / dist1[point1];
						bottom[1][point1][point2] = 1.0;
					} else if (envelopeHigh >= 0 & envelopeLow <= dist1[point1]) {
						// If the envelope is completely within the line.
						bottom[0][point1][point2] = 1.0 * envelopeLow / dist1[point1];
						bottom[1][point1][point2] = 1.0 * envelopeHigh / dist1[point1];
					}

				}
			}
		}

//		System.out.println("The bottom array.........."); 
//		for(int y= 0;y<length1-1;y++){ for(int k= 0;k<length2;k++){
//			System.out.print(" "+bottom[0][y][k]); } System.out.println(); 
//		}
//		for(int y= 0;y<length1-1;y++){ for(int k= 0;k<length2;k++){
//			System.out.print(" "+bottom[1][y][k]); } System.out.println(); 
//		}

		return bottom;
	}


	/**
	 * Computes the Free Space of traj1 with respect to traj2
	 * 
	 * @param traj1
	 * 				first input trajctory
	 * @param traj2
	 * 				Second input trajctory
	 * @param dist1
	 * 				distance between consecutives point of traj1
	 * @param dist2
	 * 				distance between consecutives point of traj2
	 */
	public static Double[][][] computeFreeSpaceLeft2(Trajectory traj1, Trajectory traj2, double[] dist2,
			double[][] distMatrixSq12, double leashSq) {
		// Creating a unit vector in the direction of the next point from point1.
		Point unitVector1 = new GeometryFactory().createPoint(new Coordinate(0, 0));
		Point vector12 = new GeometryFactory().createPoint(new Coordinate(0, 0));


		int length1 = traj1.size();
		int length2 = traj2.size();
		Double[][][] left = new Double[2][length1][length2 - 1];

		// initialize the bottom Array
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < length1; j++) {
				for (int k = 0; k < length2 - 1; k++) {
					left[i][j][k] = -1.0;
				}
			}
		}

		for (int point2 = 0; point2 < (length2 - 1); point2++) {
			if (dist2[point2] != 0) {
				//				unitV1.setLocation(1.0 * (traj2.get(point2 + 1).getX() - traj2.get(point2).getX()) / (dist2[point2]),
				//						1.0 * (traj2.get(point2 + 1).getY() - traj2.get(point2).getY()) / (dist2[point2]));
				double uv_i = 1.0 * (traj2.get(point2 + 1).getX() - traj2.get(point2).getX()) / (dist2[point2]);
				double uv_j = 1.0 * (traj2.get(point2 + 1).getY() - traj2.get(point2).getY()) / (dist2[point2]);
				unitVector1 = new GeometryFactory().createPoint(new Coordinate(uv_i,uv_j));
				// System.out.println("The unit Vector is "+unitV1);
			}
			for (int point1 = 0; point1 < length1; point1++) {
				// Creating a vector from point1 to point 2.
				//				vect12.setLocation(traj1.get(point1).getX() - traj2.get(point2).getX(),
				//						traj1.get(point1).getY() - traj2.get(point2).getY());
				double uv_i = traj1.get(point1).getX() - traj2.get(point2).getX();
				double uv_j = traj1.get(point1).getY() - traj2.get(point2).getY();
				vector12 = new GeometryFactory().createPoint(new Coordinate(uv_i, uv_j));
				// System.out.println("The vect12 is "+vect12);
				// Dot product finds how far from point1 the closest point on
				// the line is.
				double pointDist = unitVector1.getX() * vector12.getX() + unitVector1.getY() * vector12.getY();
				// System.out.println("POint dist"+ pointDist);
				// Squaring for easy calculations
				double pointDistSq = pointDist * pointDist;
				// The square of the distance between the line segment and the
				// point.
				double shortDist = distMatrixSq12[point1][point2] - pointDistSq;
				// System.out.println("SHort dist "+ shortDist);
				// If some part of the current line can be used by the leash.
				if (shortDist <= leashSq) {
					// Calculating the envelope along the line.
					double envSize = Math.sqrt(leashSq - shortDist);
					double envelopeLow = pointDist - envSize;
					double envelopeHigh = pointDist + envSize;
					// If the whole line is within the envelope.
					if (envelopeHigh >= dist2[point2] & envelopeLow <= 0) {
						left[0][point1][point2] = 0.0;
						left[1][point1][point2] = 1.0;
					} else if (envelopeHigh >= 0 & envelopeLow <= 0) {
						// If the start of the line is within the envelope.
						left[0][point1][point2] = 0.0;
						left[1][point1][point2] = 1.0 * envelopeHigh / dist2[point2];
					} else if (envelopeHigh >= dist2[point2] & envelopeLow <= dist2[point2]) {
						// If the end of the line is within the envelope.
						left[0][point1][point2] = 1.0 * envelopeLow / dist2[point2];
						left[1][point1][point2] = 1.0;
					} else if (envelopeHigh >= 0 & envelopeLow <= dist2[point2]) {
						// If the envelope is completely within the line.
						left[0][point1][point2] = 1.0 * envelopeLow / dist2[point2];
						left[1][point1][point2] = 1.0 * envelopeHigh / dist2[point2];
					}

				}
			}
		}
		/*
		 * System.out.println("The left array.........."); for(int y=
		 * 0;y<length1;y++){ for(int k= 0;k<length2 - 1 ;k++){
		 * System.out.print(" "+left[0][y][k]); } System.out.println(); }
		 * 
		 * for(int y= 0;y<length1;y++){ for(int k= 0;k<length2 - 1;k++){
		 * System.out.print(" "+left[1][y][k]); } System.out.println(); }
		 */
		return left;
	}


	/**
	 * Computes the leash length between 1 2d-point and a trajectory, in case traj1 or traj2's length is 1.
	 * 
	 * @param traj1
	 * 			first input trajectory
	 * @param traj2
	 * 			second input trajectory
	 * @return
	 * @throws Exception
	 */
	public static double singlePointCalc2(Trajectory traj1, Trajectory traj2) throws Exception {
		int length1 = traj1.size();
		int length2 = traj2.size();
		double leashSq = -1.0;
		// Calculating each leash possibility.
		if (length1 == 1) {
			for (int point2 = 0; point2 < length2 - 1; point2++) {
				double newLeashSq = distanceSquared(traj1.get(0), traj2.get(point2));
				// Keeping the new leash if it is longer than the previous.
				if (newLeashSq > leashSq) {
					leashSq = newLeashSq;
				}
			}
		} else if (length2 == 1) {
			for (int point1 = 0; point1 < length1 - 1; point1++) {
				double newLeashSq = distanceSquared(traj1.get(point1), traj2.get(0));
				// Keeping the new leash if it is longer than the previous.
				if (newLeashSq > leashSq) {
					leashSq = newLeashSq;
				}
			}
		}
		// Returning the leash.
		if (leashSq >= 0) {
			return (Math.sqrt(leashSq));
		} else {
			throw new Exception("Error in single point trajectory calculation.");
		}
	}
	/**
	 * 
	 * @param length1
	 * @param length2
	 * @param bottom
	 * @param left
	 * @param newBot
	 * @param newLeft
	 * @return
	 */
	public static Pair<Double[][],Double[][]>  computeMonotoneFreeSpace(int length1, int length2, Double[][][] bottom,
			Double[][][] left, Double[][][] newBot, Double[][][] newLeft) {
		// Calculating the monotone freespace
		for (int point1 = 0; point1 < length1; point1++) {
			for (int point2 = 0; point2 < length2; point2++) {
				if (point1 != length1 - 1 & point2 != 0) {
					// If the area is allowable from the freespace.
					if (bottom[0][point1][point2] > -0.1) {
						// If the area can be entered from the left.
						if (newLeft[0][point1][(point2 - 1)] > -0.1) {
							// Setting the monotone freespace for these points.
							newBot[0][point1][point2] = bottom[0][point1][point2];
							newBot[1][point1][point2] = bottom[1][point1][point2];
						} else if (newBot[0][point1][(point2 - 1)] > -0.1) {
							// Otherwise using the new bottom array.
							// Setting the monotone freespace according to what
							// is reachable.
							if (newBot[0][point1][(point2 - 1)] <= bottom[0][point1][point2]) {
								newBot[0][point1][point2] = bottom[0][point1][point2];
								newBot[1][point1][point2] = bottom[1][point1][point2];
							} else if (newBot[0][point1][(point2 - 1)] <= bottom[1][point1][point2]) {
								newBot[0][point1][point2] = newBot[0][point1][(point2 - 1)];
								newBot[1][point1][point2] = bottom[1][point1][point2];
							}
						}
					}
				}
				if (point2 != length2 - 1 & point1 != 0) {
					// If the area is allowable from the freespace.
					if (left[0][point1][point2] > -0.1) {
						// If the area can be entered from below.
						if (newBot[0][(point1 - 1)][point2] > -0.1) {
							// Setting the monotone freespace for these points.
							newLeft[0][point1][point2] = left[0][point1][point2];
							newLeft[1][point1][point2] = left[1][point1][point2];
						} else if (newLeft[0][(point1 - 1)][point2] > -0.1) {
							// Otherwise using the new left array.
							// Setting the monotone freespace according to what
							// is reachable.
							if (newLeft[0][point1 - 1][point2] <= left[0][point1][point2]) {
								newLeft[0][point1][point2] = left[0][point1][point2];
								newLeft[1][point1][point2] = left[1][point1][point2];
							} else if (newLeft[0][(point1 - 1)][point2] <= left[1][point1][point2]) {
								newLeft[0][point1][point2] = newLeft[0][(point1 - 1)][point2];
								newLeft[1][point1][point2] = left[1][point1][point2];
							}
						}
					}
				}
			}
		}
		Pair<Double[][],Double[][]> leftbottom = new Pair<Double[][], Double[][]>(newLeft[1], newBot[1]);
		return leftbottom;
	}

	public static Double[][] InitializeArray(Double[][] array) {
		for (int j = 0; j < array[0].length; j++) {
			for (int k = 0; k < array.length; k++) {
				array[j][k] = -1.0;
			}
		}
		return array;
	}
	
    public static double distanceSquared(Point p1 , Point p2) {
        double dx = p1.getX() - p2.getX();
        double dy = p1.getY() - p2.getY();
        return dx * dx + dy * dy;
    }
}
