package util;

import java.util.HashSet;
import java.util.Set;

import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.index.strtree.STRtree;

public class DualRTree {

	STRtree startPointRTree = null;
	STRtree endPointRTree = null;
	
	public DualRTree(){
		startPointRTree = new STRtree();
		endPointRTree = new STRtree();
	}
	
	public void insert(Point startPoint, Point endPoint, String trajId){
		addStartPoint(startPoint, trajId);
		addEndPoint(endPoint, trajId);
	}
	
	public void addStartPoint(Point startPoint, String trajId){
		startPointRTree.insert(new Envelope(startPoint.getCoordinate()), trajId);
	}
	
	public void addEndPoint(Point endPoint, String trajId){
		endPointRTree.insert(new Envelope(endPoint.getCoordinate()), trajId);
	}
	
	public Set<String> queryStartPoint(Point startPoint, double bound){
		Envelope env = startPoint.getEnvelopeInternal();
		env.expandBy(bound);
		HashSet<String> startPointResults = new HashSet<String>(startPointRTree.query(env));
		return startPointResults;
	}
	
	public Set<String> queryEndPoint(Point endPoint, double bound){
		Envelope env = endPoint.getEnvelopeInternal();
		env.expandBy(bound);
		HashSet<String> endPointResults = new HashSet<String>(endPointRTree.query(env));
		return endPointResults;
	}
	
	public Set<String> query(Point startPoint, Point endPoint, double bound){
		Set<String> startPointResults = queryStartPoint(startPoint, bound);
		Set<String> endPointResults = queryEndPoint(endPoint, bound);
		startPointResults.retainAll(endPointResults);
		return startPointResults;
	}
	
}
