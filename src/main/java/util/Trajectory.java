package util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.simplify.DouglasPeuckerSimplifier;

import core.DataHolder;
import frechet.ContinuousFrechet;

public class Trajectory {

	double[][] points = null;

	public Trajectory(double[][] pointSequence) {
		if (!(pointSequence == null)) {
			points = new double[pointSequence.length][2];
			for (int i = 0; i < pointSequence.length; i++) {
				points[i] = pointSequence[i].clone();
			}
		}
	}

	public Trajectory(Coordinate[] coordinateSequence) {
		if (!(coordinateSequence == null)) {
			points = new double[coordinateSequence.length][2];
			for (int i = 0; i < coordinateSequence.length; i++) {
				points[i][0] = coordinateSequence[i].x;
				points[i][1] = coordinateSequence[i].y;
			}
		}
	}

	public Point get(int index) {

		return new GeometryFactory().createPoint(new Coordinate(points[index][0], points[index][1]));

	}

	public int size() {
		return points.length;
	}

	public static double getLongestEdge(Trajectory traj1) {
		if (traj1.points == null) {
			return -1.0;
		} else if (traj1.points.length == 1) {
			return 0.0;
		} else {
			double longestEdgeSquared = 0.0;
			for (int i = 0; i < traj1.points.length - 1; i++) {
				double x1 = traj1.points[i][0];
				double x2 = traj1.points[i + 1][0];
				double y1 = traj1.points[i][1];
				double y2 = traj1.points[i + 1][1];

				double tmpSquared = (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2);
				if (tmpSquared > longestEdgeSquared) {
					longestEdgeSquared = tmpSquared;
				}
			}
			return Math.sqrt(longestEdgeSquared);
		}

	}

	public Point getStartPoint() {
		return new GeometryFactory().createPoint(new Coordinate(points[0][0], points[0][1]));
	}

	public Point getEndPoint() {
		return new GeometryFactory()
				.createPoint(new Coordinate(points[points.length - 1][0], points[points.length - 1][1]));
	}

	public Coordinate[] getCoordinates() {
		Coordinate[] coordinates = new Coordinate[points.length];

		int i = 0;
		for (double[] point : points)
			coordinates[i++] = new Coordinate(point[0], point[1]);

		return coordinates;
	}
/*	
	private static double findEpsilonError(double errorRate) {
		List<Double> boundlist = new ArrayList<Double>(DataHolder.queryBoundsMap.values());
		Collections.sort(boundlist);
		return boundlist.get(boundlist.size() / 2) * errorRate;
	}
	/**
	 * 
	 * @param errorbound
	 * @return
	 */

//	public static void createSimplifiedTrajectories() {
//		// Find the Epsilon simplification factor
//		System.out.println("Applying the Simplification Filter... \n");
//		double epsilon = DataHolder.simplificationError;
//		for (String trajid : DataHolder.trajectoryMap.keySet()) {
//			Trajectory tmp = DataHolder.trajectoryMap.get(trajid);
//
//			LineString trajline = new GeometryFactory().createLineString(tmp.getCoordinates());
//			LineString simplifiedtrajline = (LineString) DouglasPeuckerSimplifier.simplify(trajline, epsilon);
//			
//			
//			Trajectory simplified = new Trajectory(simplifiedtrajline.getCoordinates());
//
//			try {
//				if(!ContinuousFrechet.frechetDecision(tmp, simplified, DataHolder.simplificationError)){
//					System.out.println("ERROR IN SIMPLIFICATION!!" + trajid + " " + simplifiedtrajline.getNumGeometries());
//					System.out.println(trajline);
//					System.out.println(simplifiedtrajline);
//				}
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//				
//			
//			DataHolder.simplifiedTrajectoryMap.put(trajid, simplified);
////			System.out.println(trajid);
////			System.out.println("\t\t\t\tThe Original Length::" + trajline.getCoordinates().length);
////			System.out.println("\t\t\t\t The Simplified Length::" + simplifiedtrajline.getCoordinates().length);
//		}
//	}
		
}
