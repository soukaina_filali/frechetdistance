package test.java;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import com.vividsolutions.jts.geom.Coordinate;

import frechet.DiscreteFrechet;
import util.Trajectory;

public class DiscreteFrechetDistanceTest {

	DiscreteFrechet frechet = new DiscreteFrechet();

	@Test
	public void testComputeDiscreteFrechet() {

		Coordinate[] coordinateSequence1 = new Coordinate[20];
		Coordinate[] coordinateSequence2 = new Coordinate[30];

		for (int i = 0; i < 20; i++)
			coordinateSequence1[i] = new Coordinate(i, i + 4);
		for (int i = 0; i < 30; i++)
			coordinateSequence2[i] = new Coordinate(i, i);

		Trajectory traj1 = new Trajectory(coordinateSequence1);
		Trajectory traj2 = new Trajectory(coordinateSequence2);

		double res = 11.661903789690601;

		assertTrue(DiscreteFrechet.computeDiscreteFrechet(traj1, traj2) == res);
	}

	@Test
	public void testdiscreteFrechetDecision() {

		Coordinate[] coordinateSequence1 = new Coordinate[20];
		Coordinate[] coordinateSequence2 = new Coordinate[30];

		for (int i = 0; i < 20; i++)
			coordinateSequence1[i] = new Coordinate(i, i + 4);
		for (int i = 0; i < 30; i++)
			coordinateSequence2[i] = new Coordinate(i, i);

		Trajectory traj1 = new Trajectory(coordinateSequence1);
		Trajectory traj2 = new Trajectory(coordinateSequence2);

		boolean res = false;

		assertTrue(DiscreteFrechet.discreteFrechetDecision(traj1, traj2, 10) == res);
	}
}
