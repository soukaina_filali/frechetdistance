import static org.junit.Assert.*;

import java.awt.geom.Point2D;
import java.util.ArrayList;

import frechet.ContinuousFrechet;
import util.Pair;
import util.Trajectory;
import org.junit.Test;

import com.vividsolutions.jts.geom.Point;

public class ContinuousFrechetTest {
	ContinuousFrechet frechet = new ContinuousFrechet();

	@Test
	public void testValidateTrajectories() throws Exception {

		ArrayList<Point2D> traj1 = new ArrayList<>();
		ArrayList<Point2D> traj2 = new ArrayList<>();

		for (int i = 0; i < 20; i++)
			traj2.add(new Point2D.Double(i, i + 4));
		for (int i = 0; i < 30; i++)
			traj1.add(new Point2D.Double(i, i));

		double[][] distSq12 = new double[traj1.size()][traj2.size()];
		int length1 = traj1.size();
		int length2 = traj2.size();

		for (int i = 0; i < length1; i++) {
			for (int j = 0; j < length2; j++) {
				distSq12[i][j] = traj2.get(j).distanceSq(traj1.get(i));
			}
		}
		assertTrue(ContinuousFrechet.validateTrajectories(distSq12, 1.0, length1, length2) == false);
	}

	@Test
	public void testcomputeFreeSpaceLeft() {
		double[][] t1 = new double[5][2];
		double[][] t2 = new double[4][2];
		for (int i = 0; i < 5; i++) {
			t1[i][0] = i;
			t1[i][1] = i;
		}
		for (int i = 0; i < 4; i++) {
			t2[i][0] = i;
			t2[i][1] = i + 4;
		}

		Trajectory traj1 = new Trajectory(t1);
		Trajectory traj2 = new Trajectory(t2);

		double[][] distSq12 = new double[traj1.size()][traj2.size()];
		int length1 = traj1.size();
		int length2 = traj2.size();

		double[] dist2 = new double[length2 - 1];
		for (int i = 0; i < length2 - 1; i++) {
			dist2[i] = traj2.get(i).distance(traj2.get(i + 1));
		}

		for (int i = 0; i < length1; i++) {
			for (int j = 0; j < length2; j++) {
				distSq12[i][j] = distanceSquared(traj2.get(j), traj1.get(i));
			}
		}
		Double[][][] ans = {
				{ { -1.0, -1.0, -1.0 }, { 0.0, -1.0, -1.0 }, { 0.0, 0.0, -1.0 }, { 0.0, 0.0, 0.0, },
						{ 0.12917130661302934, 0.0, 0.0 }, },
				{ { -1.0, -1.0, -1.0 }, { 0.8708286933869708, -1.0, -1.0 }, { 1.0, 0.8708286933869708, -1.0 },
						{ 1.0, 1.0, 0.8708286933869708 }, { 1.0, 1.0, 1.0 } } };

		assertTrue(java.util.Arrays
				.deepEquals(ContinuousFrechet.computeFreeSpaceLeft2(traj1, traj2, dist2, distSq12, 15), ans));
	}

	@Test
	public void testcomputeFreeSpaceBottom() {
		double[][] t1 = new double[5][2];
		double[][] t2 = new double[4][2];
		for (int i = 0; i < 5; i++) {
			t1[i][0] = i;
			t1[i][1] = i;
		}
		for (int i = 0; i < 4; i++) {
			t2[i][0] = i;
			t2[i][1] = i + 4;
		}

		Trajectory traj1 = new Trajectory(t1);
		Trajectory traj2 = new Trajectory(t2);
		//
		// ArrayList<Point2D> traj1 = new ArrayList<>();
		// ArrayList<Point2D> traj2 = new ArrayList<>();
		//
		// for(int i=0;i<4;i++)
		// traj2.add(new Point2D.Double (i, i+4));
		// for(int i=0;i<5;i++)
		// traj1.add(new Point2D.Double (i, i));
		//
		double[][] distSq12 = new double[traj1.size()][traj2.size()];
		int length1 = traj1.size();
		int length2 = traj2.size();

		double[] dist1 = new double[length1 - 1];

		for (int i = 0; i < length1 - 1; i++) {
			dist1[i] = traj1.get(i).distance(traj1.get(i + 1));
		}

		for (int i = 0; i < length1; i++) {
			for (int j = 0; j < length2; j++) {
				distSq12[i][j] = distanceSquared(traj2.get(j), traj1.get(i));
			}
		}
		Double[][][] ans = {
				{ { 0.12917130661302934, -1.0, -1.0, -1.0 }, { 0.0, 0.12917130661302934, -1.0, -1.0 },
						{ 0.0, 0.0, 0.12917130661302934, -1.0 }, { 0.0, 0.0, 0.0, 0.12917130661302934 } },
				{ { 1.0, -1.0, -1.0, -1.0 }, { 1.0, 1.0, -1.0, -1.0 }, { 1.0, 1.0, 1.0, -1.0 },
						{ 0.8708286933869708, 1.0, 1.0, 1.0 } } };

		assertTrue(java.util.Arrays
				.deepEquals(ContinuousFrechet.computeFreeSpaceBottom2(traj1, traj2, dist1, distSq12, 15), ans));
	}

	@Test
	public void testsinglePointCalc() throws Exception {
		double[][] t1 = new double[1][2];
		double[][] t2 = new double[4][2];
		for (int i = 0; i < 1; i++) {
			t1[i][0] = 1;
			t1[i][1] = 1;
		}
		for (int i = 0; i < 4; i++) {
			t2[i][0] = i;
			t2[i][1] = i + 4;
		}

		Trajectory traj1 = new Trajectory(t1);
		Trajectory traj2 = new Trajectory(t2);

		int length1 = traj1.size();
		int length2 = traj2.size();
		double[][] distSq12 = new double[traj1.size()][traj2.size()];

		for (int i = 0; i < length1; i++) {
			for (int j = 0; j < length2; j++) {
				distSq12[i][j] = distanceSquared(traj2.get(j), traj1.get(i));
			}
		}

		assertTrue(ContinuousFrechet.singlePointCalc2(traj1, traj2) == 5.0990195135927845);
	}

	@Test
	public void testcomputeMonotoneFreeSpace() {
		ArrayList<Point2D> traj1 = new ArrayList<>();
		ArrayList<Point2D> traj2 = new ArrayList<>();

		for (int i = 0; i < 4; i++)
			traj2.add(new Point2D.Double(i, i + 4));
		for (int i = 0; i < 5; i++)
			traj1.add(new Point2D.Double(i, i));

		double[][] distSq12 = new double[traj1.size()][traj2.size()];
		int length1 = traj1.size();
		int length2 = traj2.size();

		double[] dist2 = new double[length2 - 1];
		for (int i = 0; i < length2 - 1; i++) {
			dist2[i] = traj2.get(i).distance(traj2.get(i + 1));
		}

		for (int i = 0; i < length1; i++) {
			for (int j = 0; j < length2; j++) {
				distSq12[i][j] = traj2.get(j).distanceSq(traj1.get(i));
			}
		}

		Double[][][] newBot = new Double[2][length1 - 1][length2];
		Double[][][] newLeft = new Double[2][length1][length2 - 1];
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < length1 - 1; j++) {
				for (int k = 0; k < length2; k++) {
					newBot[i][j][k] = -1.0;
				}
			}
		}

		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < length1; j++) {
				for (int k = 0; k < length2 - 1; k++) {
					newLeft[i][j][k] = -1.0;
				}
			}
		}
		Double[][][] bottom = {
				{ { 0.12917130661302934, -1.0, -1.0, -1.0 }, { 0.0, 0.12917130661302934, -1.0, -1.0 },
						{ 0.0, 0.0, 0.12917130661302934, -1.0 }, { 0.0, 0.0, 0.0, 0.12917130661302934 } },
				{ { 1.0, -1.0, -1.0, -1.0 }, { 1.0, 1.0, -1.0, -1.0 }, { 1.0, 1.0, 1.0, -1.0 },
						{ 0.8708286933869708, 1.0, 1.0, 1.0 } } };
		Double[][][] left = {
				{ { -1.0, -1.0, -1.0 }, { 0.0, -1.0, -1.0 }, { 0.0, 0.0, -1.0 }, { 0.0, 0.0, 0.0, },
						{ 0.12917130661302934, 0.0, 0.0 }, },
				{ { -1.0, -1.0, -1.0 }, { 0.8708286933869708, -1.0, -1.0 }, { 1.0, 0.8708286933869708, -1.0 },
						{ 1.0, 1.0, 0.8708286933869708 }, { 1.0, 1.0, 1.0 } } };

		Pair<Double[][], Double[][]> leftbottom = ContinuousFrechet.computeMonotoneFreeSpace(length1, length2, bottom,
				left, newBot, newLeft);

		assertTrue(java.util.Arrays.deepEquals(leftbottom.first,
				new Double[][] { { -1.0, -1.0, -1.0 }, { -1.0, -1.0, -1.0 }, { -1.0, -1.0, -1.0 }, { -1.0, -1.0, -1.0 },
						{ -1.0, -1.0, -1.0 } })
				&& java.util.Arrays.deepEquals(leftbottom.second, new Double[][] { { -1.0, -1.0, -1.0, -1.0 },
						{ -1.0, -1.0, -1.0, -1.0 }, { -1.0, -1.0, -1.0, -1.0 }, { -1.0, -1.0, -1.0, -1.0 } }));

	}

	public static double distanceSquared(Point p1, Point p2) {
		double dx = p1.getX() - p2.getX();
		double dy = p1.getY() - p2.getY();
		return dx * dx + dy * dy;

	}
}
