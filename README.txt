﻿/***********************************************************************
	File: README.txt
	Date: 7/29/2017
	
/***********************************************************************

Our project is organized as follows:
	/files.zip
	/pom.xml
	/Results
	/src
	../main.java
	.../DataHolder
	.../ParallelQueryService
	.../QueryExecutor
	../frechet
	.../ContinuousFrechet
	.../DiscreteFrechet
	../util
	.../DualRTree
	.../Pair	
	.../Trajectory
	../test.java
	.../ContinuousFrechetTest
	./Runner

To install this project maven is required. Essentially using the pom.xml file, we create a runnable JAR file.
This is a convenient way to solve the dependencies and easily compile&package the source code. 

From command line, go to the project directory.
>> cd {PROJECT_DIRECTORY}
Then, install the project using maven.
>> mvn install
The created JAR file wil lbe located under target folder.
You can run the packaged JAR file as follows:
>> java -jar target/edu.gsu.dmlab.frechet-giscup-jar-with-dependencies.jar 
 
An example of dataset.txt, queries.txt and trajectory data folder(/files) are contained in the root of the project directory.

To change the path of the files, specify the new path in "pathPrefix" (line 35, Runner.class). The result files are generated
in the /Results directory.

Our idea is to solve the Continuous Frechet decision problem using the geodesic free-space diagram parametric approach from [1].
Before that, we applied a series of inexpensive filters on the trajectories. The filters are applied in an increasing order of 
complexity. The first is a dual R-tree on the trajectories start and end points using the Sort-Tile-Recursive (STR) algorithm[2].
The idea is to filter out all the trajectories whose start and end points fall in a distance greater than the query bound with 
respect to the query trajectory. The second filter is the the Discrete Frechet distance since, theoretically, there is a guarantee
that [ContinuousFrechet <= DiscreteFrechet  and DiscreteFrechet  <= ContinuousFrechet + maxEdgeLength]. For additional speedup,
we are processing the queries in parallel.



Reference:
[1]Alt, H. and Godau, M. (1995) Computing the Frechet distance between two polygonal curves.
International Journal of Computational Geometry & Applications, 5(01n02), 75�91

[2]P. Rigaux, Michel Scholl and Agnes Voisard. Spatial Databases With Application To GIS. 
Morgan Kaufmann, San Francisco, 2002.
	
	